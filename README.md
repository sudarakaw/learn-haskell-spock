# Practice Code: Spock - Haskell Web Framework

Practice code created while learning [Spock](https://www.spock.li/), a
[Haskell](https://www.haskell.org/) web server framework.

## Projects

**1. notekeeper** - Example project from [Your First Web Application with Spock](https://haskell-at-work.com/episodes/2018-04-09-your-first-web-application-with-spock.html)
by [Oskar Wickström](https://wickstrom.tech/).
