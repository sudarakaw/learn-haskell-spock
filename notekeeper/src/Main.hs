-- src/Main.hs:
--

{-# LANGUAGE OverloadedStrings #-}


module Main where


import           Control.Monad                  ( forM_ )
import           Control.Monad.IO.Class         ( liftIO )
import           Data.IORef
import           Data.Semigroup                 ( (<>) )
import           Data.Text                      ( Text )
import           Lucid
import           Web.Spock
import           Web.Spock.Config
import           Web.Spock.Lucid                ( lucid )


-- MODEL

type Server a = SpockM () () ServerState a

newtype ServerState = ServerState {notes :: IORef [Note]}

data Note = Note
  { author   :: Text
  , contents :: Text
  }


-- APP

app :: Server ()
app = do
  get root $ do
    notes' <- getState >>= (liftIO . readIORef . notes)

    lucid $ do
      h1_ "Notes"

      ul_ $ forM_ notes' $ \note -> li_ $ do
        toHtml (author note)
        ": "
        toHtml (contents note)

      h2_ "New Note"
      form_ [method_ "post"] $ do
        label_ $ do
          "Author: "
          input_ [name_ "author"]
        label_ $ do
          "Contents: "
          textarea_ [name_ "contents"] ""
        button_ [type_ "submit"] "Add Note"

  post root $ do
    author   <- param' "author"
    contents <- param' "contents"
    noteRef  <- notes <$> getState
    liftIO $ atomicModifyIORef' noteRef $ \notes ->
      (notes <> [Note author contents], ())

    redirect "/"


-- MAIN

main :: IO ()
main = do
  st <- ServerState <$> newIORef
    [ Note "Alice" "Must not forget to walk the dog."
    , Note "Bob"   "Must. Eat. Pizza!"
    ]
  cfg <- defaultSpockCfg () PCNoDatabase st

  runSpock 5000 (spock cfg app)
